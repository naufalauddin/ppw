from django.urls import path
from django.shortcuts import render

from . import views

urlpatterns = [
    path('', views.front_page, name='front_page'),
    path('about_me', views.about_me, name='about_me'),
    path('skill', views.skill, name='skill'),
    path('experiment', views.experiment, name='experiment'),
    path('contacts', views.contacts, name='contacts'),
]
